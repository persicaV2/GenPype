#!/bin/bash

function usage()
{

echo "USAGE : pipeline [-h|--help]

  OPTIONS:
  -h|--help)
  Print this help and exit

  -i|--input)
  The folder containing the files you want to analyze

  -d|--data)
  The format/extension of the files you want to analyze.
  You can choose between sra and fastq. Working on fastq.gz
  fastq are supposed to match the convention for .sra files
  eg: MyFile_1.fastq MyFile_2.fastq

  -o|--outdir)
  The output directory of your data

  -r|--refgen)
  The reference genome to be used.
  Please note that in the same folder you must have the index files
	
  -u|--cpu)
  Cores needed

  --minlen)
  Minimal length to retain reads during trimming.
  Default = 36

  -f|--force)
  overwrite every previous resoluts in the output folder

  -a|--account)
  Mandatory. Qsub parameter. Must be a valid account name.

  -q|--queue)
  Qsub parameter. Default = parallel.

  --memory)
  Qsub parameter. Inser the amount of RAM you need.

  -w|--walltime)
  Qsub parameter

  -b|--buffer)
  Number of jobs that will be submitted in the same time. Default 3.

"
}

# se non ci sono parametri nella riga di comando stampa usage ed esce
if [ "$#" -lt 1 ]
then
    echo "[ERROR] You did not provide any option."
    usage
    exit 1
fi

# definisco le variabili di cui avrò bisogno in seguito
# input file e output path

export input=""
export type=""
export outdir=""

#reference genome
export refgenome=""

#Qsub related parameters
##########################
export walltime=""
export memory=""
export cpu=""
export account=""
export queue="" 
#########################

#numero di job contemporaneamente in coda/in corsa
export buffer=""

#--variabili per i tools--

#lunghezza minima accettabile nel trimming
export ml=""

# read the options
# ricordiamo che una lettera seguita da ":" e' obbligatoria altrimenti opzionale

TEMP=`getopt -o i:d:o:r:a:q:u:w:b:fh -l input:,data_type:,outdir:,refgen:,account:,queue:,cpu:,memory:,walltime:,buffer:,minlen:,force,help -n -- 'pipeline_bone.sh' "$@"`

#valuta il set contenuto in TEMP
eval set -- "$TEMP"

#Estrae le opzioni e i loro argmenti

while true; do

    case "$1" in
	
	-h|--help)
	    usage ; exit 0 ;;
	
	    #files
	-i|--input)
	    input=$2 ; shift 2 ;;
	
	-d|--data)
	    type=$2 ; shift 2 ;;

	-o|--outdir)
	    outdir=$2 ; shift 2 ;;
	
	-r|--refgen)
	    refgenome=$2 ; shift 2 ;;
	
	-f|--force)
	    rm -rf $outdir/* ; mkdir $outdir ; shift 1 ;;

	#parametri per tools

	--minlen)
	    ml=$2 ; shift 2 ;;
	# fine parametri per tool
	
	#Qsub related parameters
	
	-a|--account)
	    account=$2 ; shift 2 ;;

	-q|--queue)
	    queue=$2 ; shift 2 ;;   
	
	-u|--cpu)
	    cpu=$2 ; shift 2 ;;  

	--memory)
	    memory=$2 ; shift 2 ;;

	-w|--walltime)
	    walltime=$2 ; shift 2 ;;

	-b|--buffer)
	    buffer=$2; shift 2 ;;	
	
	--) shift ; break ;;

	*) echo "Internal error!" ; exit 1 ;;
    esac
done


    #------CONTROLLO PARAMETRI-------
    #-- Files e Paths--

    #Input
if [ -z "$input" ]; then
    echo "Please, specify the input of your data (-i|--input)"
    usage
    exit
fi

if [ ! -d "$input" ]; then
    echo "Input should be a directory!"
    usage
    exit
fi


if [ -z "$type" ]; then
    echo "Please, specify the data input: sra, fastq (working on fastq.gz)"
fi

if [ "$type" == 'sra' ] || [ "$type" == 'fastq' ]; then

    echo "Branch for $type format"
else
    echo "Chose a file format between 'sra','fastq','fastq.gz'"
fi
    # Se non c'è il path dell'output
if [ -z "$outdir" ]; then
    echo "You did not create specified an outpt folder"
    echo "I will create a folder called out"
        echo "Continue? [Y/N]"
    read choiche
    case $choiche in
	y|Y)
	    echo "creating folder out_$now";
	    mkdir $PWD/out;
	    outdir=$PWD/out;;
	n|N)
	    echo "Stopping work"; usage ; exit ;;
    esac
else
    # Se il path dell'output è specificato ma non è una directory
    if [ ! -d "$outdir" ]; then
	echo "You did not specified an existing folder"
       
	echo "I will create a folder called out_$now"
        echo "Continue? [Y/N]"
	read choiche
	case $choiche in
	    y|Y)
		echo "creating folder out";
		mkdir $PWD/out;
		outdir=$PWD/out;;
	    n|N)
		echo "Stopping work"; usage ; exit ;;
	esac
    fi
fi

# Se esiste il genoma di riferimento
if [ -z "$refgenome" ]; then
    echo "Please, specify the reference genome: it's necessary for the bwa step"
    usage
    exit
fi

    #--Parametri PBS--

    #Account name
if [ -z "$account" ]; then
    echo "Specify a valid account for PBS"
    usage
    exit
fi

    #Numeri core
    #Va bene solo per PICO
if [ -z "$cpu" ]; then
    echo "Specify the number of cores you want to use"
    usage
    exit
else    
    if [ $cpu -le 20 ]; then
	:
    else
	"Too many cores requested!"
    fi
fi

    #Memoria
    #Va bene solo per PICO
    #probabilmente c'è un modo migliore per farlo

if [ -z "$memory" ]; then
    echo "Specify the memory you need"
    usage
    exit
else    
    if [ $memory -le 126 ]; then
	echo "Using $memory gb of memory"
    else
	if [ $queue == "bigmem" ]; then
	    if [ $memory -le 510 ]; then
		echo "Using $memory gb of memory"	
	    else
		echo "You requested too much memory: check system specification"
		exit
	    fi
	else
	    echo "You requested too much memory: check system specification"
	    exit
	fi	
    fi
fi

#Walltime
#Va fissato un limite per impedire inserimenti oltre le 100 ore o il limite della iesima macchina
if [ -z "$walltime" ]; then
    echo "Walltime information needed"
    exit
else
    if [[ "$walltime" =~ ^[0-9,:]+$ ]]; then
	echo "Using walltime (hours:minutes:seconds) $walltime" 
    else
	echo "Wrong format: check PBS specifications, option -l"
	exit
    fi
fi

#Numero di job contemporanei in coda o in corsa
if [ -z "$buffer" ]; then
    echo -e "No number of conteporary jobs spcified: \n3 jobs at time will be runned"
    echo "Continue [Y/N]"
    read choiche
    case $choiche in
	y|Y)
	    echo "Proceding with 3 jobs" ;
	    buffer=3 ;;
	n|N)
	    echo "Stopping work" ; usage ; exit ;;
    esac
fi

#--Parametri tools--

if [ -z "$ml" ]; then
    echo -e "NO minimal read length was choosen for trimming\n"
    echo "I will discard reads shorter than 36bp"
    echo "Continue? [Y/N]"
    read choiche
    case $choiche in
	y|Y)
	    echo "Trimming reads shorter than 36bp" ;
	    ml=36 ;;
	#36 è stato scelto perché trimma bene più o meno sempre con le sequenze "tipo"
	n|N)
	    echo "Stopping work"; usage ; exit ;;
    esac
fi
    #------FINE CONTROLLO------
#cartella dove vanno i file o e i di PBS

    # Preparo parametri da dare a qsub

memorygb=$memory"gb"
qsub_resources="-l select=1:ncpus=$cpu:mem=$memorygb"
qsub_walltime="-l walltime=$walltime"

qsub_queue=""
if [ ! -z "$queue" ]; then
    qsub_queue="-q $queue"
fi
 
MERGED_STEPS=""
n=0

#if it's a sra
if [ "$type" == 'sra' ]; then
    # Pipeline run
    for sample in "$input"/*.sra ; do

	#prendo solo il basename del file, dopo l'ultima barra
	input_file=$(basename $sample)

	# Prendo solo il nome del file di input non l'estensione
	export jobname=$(echo $input_file | cut -f 1 -d '.')

	export file=$sample
	
	qsub_variables="file,outdir,refgenome"
	echo "Qsub variables: " $qsub_variables
	echo "Qsub resources: "$qsub_queue" "$qsub_resources" "$qsub_walltime" "$account
	
	if [ $n -le $buffer ]; then

	    echo "-----------------"
	    echo $jobname" will be put in queue right now"
	    echo "-----------------"
	    eval "STEP[n]=$(qsub -V -A $account $qsub_resources $qsub_walltime $qsub_queue -N $jobname -v $qsub_variables ./pipeline_choice_2.sh)"
	    echo ${STEP[$n]}
	    MERGED_STEP=$MERGED_STEP:${STEP[$n]}
	    n=$((n+1))
	else
	    m=$((n-buffer))
	    echo "-----------------"
	    echo $jobname" will be put in hold"
	    echo "-----------------"
	    n=eval "STEP[n]=$(qsub -W depend=afterany:${STEP[$m]} -V -A $account $qsub_resources $qsub_walltime $qsub_queue -N $jobname -v $qsub_variables ./pipeline_choice_2.sh)"
	    MERGED_STEP=$MERGED_STEP:${STEP[$n]}
	    n=$((n+1))
	fi
    done
fi

#If it's a fastq (different menagement of job number). It can be improved for sure.
if [ "$type" == 'fastq' ]; then
    
    #is there a file with extension .fq
    count_fq=`ls -1 *.fq 2>/dev/null | wc -l`
    #is there a file with extension .fastq?
    count_fastq=`ls -1 *.fastq 2>/dev/null | wc -l`

    if [$count!=0] && [$count!=0]; then
	echo -e  "\n ATTENTION! there are both fastq and fq files!\n"
	echo "Please uniform the extensions before running the pipeline \n"
	echo "Exit now"
	exit 1
    fi

    #if so, make an array with all the .fq files found
    if [ $count_fq != 0 ]; then 
	arr=(*_1.fq)
    fi

    #if so, make an array with all the .fq files found
    if [ $count_fastq != 0 ]; then 
	arr=(*_1.fastq)
    fi



    for sample in ${arr[@]} ; do

	#prendo solo il basename del file, dopo l'ultima barra
	input_file=$(basename $sample)

	# Prendo solo il nome del file di input non l'estensione
	# Non prendo nemmeno due file fasta per uno stesso campione (_1 e _2)
	export jobname=$(echo $input_file | cut -f 1 -d '.' | sed s/\_1*$//g )

	export file=$sample
	
	qsub_variables="file,outdir,refgenome"
	echo "Qsub variables: " $qsub_variables
	echo "Qsub resources: "$qsub_queue" "$qsub_resources" "$qsub_walltime" "$account
	
	echo $sample
	echo $input_file
	echo $jobname
	
	if [ $n -le $buffer ]; then

	    echo "-----------------"
	    echo $jobname" will be put in queue right now"
	    echo "-----------------"
	    eval "STEP[n]=$(qsub -V -A $account -M i.tagliaferri@cineca.it -m bae $qsub_resources $qsub_walltime $qsub_queue -N $jobname -v $qsub_variables ./pipeline_choice_2.sh)"
	    echo ${STEP[$n]}
	    MERGED_STEP=$MERGED_STEP:${STEP[$n]}
	    n=$((n+1))
	    
	else
	    m=$((n-buffer))
	    echo "-----------------"
	    echo $jobname" will be put in hold"
	    echo "-----------------"
	    n=eval "STEP[n]=$(qsub -W depend=afterany:${STEP[$m]} -V -A $account -M i.tagliaferri@cineca.it -m bae $qsub_resources $qsub_walltime $qsub_queue -N $jobname -v $qsub_variables ./pipeline_choice_2.sh)"
	    MERGED_STEP=$MERGED_STEP:${STEP[$n]}
	    n=$((n+1))
	fi
    done
fi

echo $MERGED_STEP
#qsub -W depend=afterok$MERGED_STEP -A $account $qsub_resources $qsub_walltime $qsub_queue -N "Merging" -v $qsub_variables pipeline_tail.sh

#Nel caso si debba fare solo il merging
#qsub -A $account $qsub_resources $qsub_walltime $qsub_queue -N "Merging" -v $qsub_variables pipeline_tail.sh
