# Genotyping Pipeline

## Introduction

This pipeline is written to find genomic variants (SNPs and INDELS) in
plant genomes. Specifically this pipeline was designed to identify
variants in peach (*Prunus persica*) *cultivar* genomes.

For each cultivar, a GVCF containing called and filtered variants was
produced. Variants identified in this way were merged together in a
vcf from which SNPs and INDELs were extracted.

## How to use

The pipeline accept the following syntax:

 $ ./pipeline_bone_choiche.sh [flags] [arguments]

The flags are the following:

- -h|--help)
  Print this help and exit

- -i|--input)
  The folder containing the files you want to analyze

- -d|--data)
  The format/extension of the files you want to analyze.
  You can choose between sra and fastq. Working on fastq.gz
  fastq are supposed to match the convention for .sra files
  eg: MyFile_1.fastq MyFile_2.fastq

- -o|--outdir)
  The output directory of your data

- -r|--refgen)
  The reference genome to be used.
  Please note that in the same folder you must have the index files
  
- -u|--cpu)
  Cores needed

- --minlen)
  Minimal length to retain reads during trimming.
  Default = 36

- -f|--force)
  overwrite every previous resoluts in the output folder

- -a|--account)
  Mandatory. Qsub parameter. Must be a valid account name.

- -q|--queue)
  Qsub parameter. Default = parallel.

- --memory)
  Qsub parameter. Inser the amount of RAM you need.

- -w|--walltime)
  Qsub parameter

- -b|--buffer)
  Number of jobs that will be submitted in the same time. Default 3.

This bash script will run pipeline\_choiche.sh once for each sample
considered. This will create as many GVCFs as the samples
analysed. Variants found in all samples will be merged in a unique vcf
file by pipeline\_tail.sh.


## Dependencies

The pipeline relies on the following software:

- [sra-tools 2.8.1](https://github.com/ncbi/sra-tools/releases/tag/2.8.1)

- [Trimmomatic 0.33](http://www.usadellab.org/cms/?page=trimmomatic)

- [Bwa 0.7.15](https://github.com/lh3/bwa/releases/tag/v0.7.15)

- [Picard 2.3.0](https://github.com/broadinstitute/picard/releases/tag/2.3.0)

- [GATK 3.5] (https://software.broadinstitute.org/gatk/download/)

