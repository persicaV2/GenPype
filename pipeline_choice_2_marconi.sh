#!/bin/bash
cd $SLURM_SUBMIT_DIR
echo "Working Directoy"$(pwd)
echo "Input: "$file
echo "Output directory: "$outdir
echo "Number of bwa threads: "$bwat
echo "Reference Genome: "$refgenome
echo "Metrics: "$mtr

cat <<"EOF"
   _____                  _               _                                
  / ____|                | |             (_)                               
 | |  __  ___ _ __   ___ | |_ _   _ _ __  _ _ __   __ _                    
 | | |_ |/ _ \ '_ \ / _ \| __| | | | '_ \| | '_ \ / _` |                   
 | |__| |  __/ | | | (_) | |_| |_| | |_) | | | | | (_| |                   
  \_____|\___|_| |_|\___/ \__|\__, | .__/|_|_| |_|\__, |                   
                               __/ | |             __/ |                   
        _            _ _      |___/|_|__   ___    |___/         _      __  
       (_)          | (_)            /_ | |__ \   / / |        | |     \ \ 
  _ __  _ _ __   ___| |_ _ __   ___   | |    ) | | || |__   ___| |_ __ _| |
 | '_ \| | '_ \ / _ \ | | '_ \ / _ \  | |   / /  | || '_ \ / _ \ __/ _` | |
 | |_) | | |_) |  __/ | | | | |  __/  | |_ / /_  | || |_) |  __/ || (_| | |
 | .__/|_| .__/ \___|_|_|_| |_|\___|  |_(_)____| | ||_.__/ \___|\__\__,_| |
 | |     | |                                      \_\                  /_/ 
 |_|     |_|                                                                 
EOF
#-----------------------------------------------------------------------------------------------
#Carico i moduli
echo "---- Modules Loading ----"

#importo tutti i moduli necessari e li mando a std.output


modules=(profile/global sra/2.8.1 trimmomatic/0.36 bwa/0.7.15)
for i in ${modules[*]}; do
    module load autoload $i
    echo "I loaded "$i
done

#prendo solo il basename del file, dopo l'ultima barra
input_file=$(basename $file)
if [ "$type" = "fastq" ]; then
    # Prendo solo il nome del file di input non l'estensione nè i numeri dei fastq
    input_seed=$(echo  $input_file| cut -f 1 -d '.' | sed s/\_1*$//g )

    #echo $input_file

    echo $input_seed
else
    # Prendo solo il nome del file di input non l'estensione
    input_seed=$(echo $input_file | cut -f 1 -d '.')    
fi

outSample=$outdir/"$input_seed"_dir

#-----------------------------------------------------------------------------------------------
if [ "$type" = 'sra' ]; then

    echo -e "================================================\n" 1>&2 
    echo -e "---- 1.SPLIT.sh ---- \n"

    #Set tempo di inizio
    time1=$( date "+%s" )

    #echo $file

    # Esistono già le cartelle e i file di output?
    if [ -d $outSample ]; then
	if [ -e $outSample/"$input_seed"_1.fastq ] && [ -e $outSample/"$input_seed"_2.fastq ]; then
	    echo "The output files of this step already exist"
	    echo "$input_seed"_1.fastq
	    echo "$input_seed"_2.fastq
	    
	else
	    #fastq-dump dello strumento split-files
	    echo "I'm calling fastq-dump to produce fastq from sra file"
	    fastq-dump --split-files $input -O $outdir/"$input_seed"_dir
	    
	fi
	
    else
	#Produco una cartella col nome del file in $input
	echo "I'm creating a folder called: $input_seed _dir"
	
	mkdir -p $outdir/"$input_seed"_dir
	
	#fastq-dump dello strumento split-files
	echo "I'm calling fastq-dump to produce fastq from sra file"
	fastq-dump --split-files $file -O $outdir/"$input_seed"_dir
    fi

    time2=$( date "+%s")
    echo [elapsed time] $((($time2 - $time1)/60)) min 
    
else

echo -e "---- 1.Environment Setup ---- \n"
    if [ "$type" = "fastq" ]; then
	time1=$( date "+%s" )

	if [ -d $outdir ]; then
	    echo $outdir" exists."
	else
	    echo $outdir" does not exists"
	fi


	if [ -d $outSample ]; then
	    echo $outSample" exists."
	else
	    echo "I'm creating a folder called: $input_seed _dir"
	    mkdir $outSample
	fi

	echo "I will copy the fastq in $outSample"

    
	#is there a file with extension .fq
	count_fq=`ls -1 *.fq 2>/dev/null | wc -l`
	#if so, move them in $outsample
	if [ $count_fq != 0 ]; then 
	    cp $input/"$input_seed"_1.fq $outSample
	    cp $input/"$input_seed"_2.fq $outSample
	fi

	#is there a file with extension .fastq?
	#if so, move them in $outsample
	count_fastq=$(ls $input | grep fastq | wc -l)
	if [ $count_fastq != 0 ]; then 
	    echo $input/"$"
	    echo $input/"$input_seed"_1.fastq $outSample
	    cp $input/"$input_seed"_1.fastq $outSample
	    echo $input/"$input_seed"
	    echo $input/"$input_seed"_2.fastq $outSample
	    cp $input/"$input_seed"_2.fastq $outSample
	fi
	echo "I will skip directly to the trimming"
    fi
fi

#-----------------------------------------------------------------------------------------------
echo -e "================================================\n" 1>&2 
echo -e "---- 2.TRIM.sh ----\n"
cd $outdir

if [ -e "$input_seed"_dir/"$input_seed"_paired_forward.fastq ] && [ -e "$input_seed"_dir/"$input_seed"_paired_reverse.fastq ]; then
    #non fare nulla e vai avanti
    echo "Already trimmed results"
else
    # Chiamo trimmomatics
    echo = "Trimmomatics: trimming fasq:"
    java -jar $TRIMMOMATIC_HOME/trimmomatic-0.36.jar PE -phred33 -threads $cpu \
    "$input_seed"_dir/"$input_seed"_1.fastq \
    "$input_seed"_dir/"$input_seed"_2.fastq \
    "$input_seed"_dir/"$input_seed"_paired_forward.fastq \
    "$input_seed"_dir/"$input_seed"_unpaired_forward.fastq \
    "$input_seed"_dir/"$input_seed"_paired_reverse.fastq \
    "$input_seed"_dir/"$input_seed"_unpaired_reverse.fastq \
    ILLUMINACLIP:$TRIMM_ADAPTERS/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:$ml
fi

time3=$( date "+%s" )
echo [elapsed time] $((($time3 - $time1)/60)) min 

#-----------------------------------------------------------------------------------------------
echo -e "================================================\n" 1>&2 
echo -e "---- 3.BWA.sh ----\n"

#Esistono già i file di output?
if [ -e "$input_seed"_dir/"$input_seed".sam ]; then
    echo "Already indicized bam samples"
else
    echo "Indicizing .bam file"
    bwa mem -t $cpu -M "$refgenome" \
    "$input_seed"_dir/"$input_seed"_paired_forward.fastq \
    "$input_seed"_dir/"$input_seed"_paired_reverse.fastq \
    > "$input_seed"_dir/"$input_seed".sam
fi

time4=$( date "+%s")
echo [elapsed time] $((($time4 - $time1)/60)) min

#-----------------------------------------------------------------------------------------------
echo -e "================================================\n" 1>&2 
echo -e "----4.SORT.sh----\n"
echo "Picard: Sam sortig with SortSam. SO is set as 'Coordinate' "

#C'è un confiltto tra la versione di java caricata da trimmomatic e da picard
#Tolgo i moduli già caricati
module purge

#carico Picard
echo "I loaded picard"
module load autoload picard/2.7.1

#Per comodità mi faccio una variabile ambientale per una cartella tmp
TMP="$input_seed"_dir/tmp

#Esistono già i file di output?

if [ -e "$input_seed"_dir/"$input_seed"_sorted.bam ]; then
    echo "Already sorted the bam file"
else
    echo "Sorting .bam file"
    java -Xmx2g -Djava.io.tmpdir=$TMP \
    -jar $PICARD_HOME/picard.jar SortSam \
    I="$input_seed"_dir/"$input_seed".sam \
    O="$input_seed"_dir/"$input_seed"_sorted.bam \
    SO=coordinate
fi

time5=$( date "+%s")
echo [elapsed time] $((($time5 - $time1)/60)) min

#-----------------------------------------------------------------------------------------------
echo -e "================================================\n" 1>&2 
echo -e "----6.ADD.sh----\n"

#controllare parametri in particolare RGDI e RGSM

#Esistono già i file di output?
if [ -e "$input_seed"_dir/"$input_seed"_dedupAdd.bam ]; then
    echo "Already dereplicated the groups"
else
    echo "Dereplicating deduplicated files"
    java -Xmx2g -Djava.io.tmpdir=$TMP \
    -jar $PICARD_HOME/picard.jar AddOrReplaceReadGroups \
    I="$input_seed"_dir/"$input_seed"_sorted.bam \
    O="$input_seed"_dir/"$input_seed"_dedupAdd.bam \
    RGID=$input_file \
    RGLB=lib1 \
    RGPL=illumina \
    RGPU=unit1 \
    RGSM=$input_file
fi

time7=$( date "+%s")
echo [elapsed time] $((($time7 - $time1)/60)) min 
#----------------------------------------------------------------------------------------------
echo -e "================================================\n" 1>&2 
echo -e "----7.BUILD.sh----\n"

#controllo se ci sono già gli output
if [ -e "$input_seed"_dir/"$input_seed"_dedupAdd.bai ]; then
    echo "Already generated a the index file"
else
    echo "Generatig a bam index for the $input_seed_dedupAdd.bam file"
    java -Xmx2g -Djava.io.tmpdir=$TMP \
    -jar $PICARD_HOME/picard.jar BuildBamIndex \
    I="$input_seed"_dir/"$input_seed"_dedupAdd.bam
fi

time8=$( date "+%s")
echo [elapsed time] $((($time8 - $time1)/60)) min 


if [ -s bam_files ];then
    :
else
    mkdir bam_files
fi

echo "Coping bam files in folder $outdir"

cp "$input_seed"_dir/"$input_seed"_dedupAdd.bam ./bam_files
cp "$input_seed"_dir/"$input_seed"_dedupAdd.bai ./bam_files

#----------------------------------------------------------------------------------------------
echo -e "================================================\n" 1>&2 
echo -e "----8.HAP_CALL_multi_tread.sh----\n"

#Per conflitti della versione java eliminare il modulo picard e usare gatk
module purge

#carico gatk

modul_gatk=(profile/advanced gatk/3.6)
for i in ${modul_gatk[*]}; do
    module load autoload $i
    echo "I loaded $i"
done

if [ -e  "$input_seed"_dir/"$input_seed".g.vcf ]; then
    echo " I already produced the .vcf file"
else
    java -Djava.io.tmpdir=$TMP -Xmx32G -Xms4G \
	-jar $GATK_HOME/GenomeAnalysisTK.jar \
	-T HaplotypeCaller \
	-R ../$refgenome \
	-I "$input_seed"_dir/"$input_seed"_dedupAdd.bam \
	--emitRefConfidence GVCF \
	--variant_index_type LINEAR \
	--variant_index_parameter 128000 \
	-nct $cpu \
	-o "$input_seed"_dir/"$input_seed".g.vcf
fi

time9=$( date "+%s")
echo [elapsed time] $((($time9 - $time1)/60)) min 

if [ -s vcf_files ];then
    :
else
    mkdir vcf_files
fi

echo "Coping vcf files in folder $outdir"

cp "$input_seed"_dir/"$input_seed".g.vcf* ./vcf_files

#Comprimo la cartella con i risultati per risparmiare spazio
#NB: per evitare inconvenienti, solo se ho il vcf.idx

if [ -s "$input_seed"_dir/"$input_seed".g.vcf.idx ]; then

    #Elimino la cartella per i file temporanei di GATK
    rm -rf $TMP

    echo "Compressing output folder to save space"
    tar -zcvf "$input_seed"_dir.tar.gz "$input_seed"_dir
    rm -rf "$input_seed"_dir
    echo "Congratulations your variants are called in /vcf_files/$input_seed.g.vcf"
fi

time10=$( date "+%s")
echo [elapsed time] $((($time10 - $time1)/60)) min 


