#!/bin/bash

cd /pico/scratch/userinternal/itagliaf/pipelineMarco/otu/vcf_files

export REFGENOME=/pico/scratch/userinternal/itagliaf/pipelineMarco/reference


#importo i moduli che mi servono
module load profile/advanced
module load autoload gatk/3.5

#creo un file con la lista di tutti i g.vcf* files e ci scrivo il nome di ogni file

>vcf_list.list

for i in *vcf; do
    echo $i >> vcf_list.list
done


#--------------------------------------------------------------------------------------------------
# echo -e "----GVFC_MERGE.sh ----\n"
# echo -e "Merging all the variants found previously \n"

# TMP=tmp
# time0bis=$( date "+%s")

# java -Djava.io.tmpdir=$TMP -Xmx32G \
#     -jar $GATK_HOME/GenomeAnalysisTK.jar \
#     -T GenotypeGVCFs \
#     -R $REFGENOME/persicaV2.fasta \
#     -V vcf_list.list \
#     -stand_call_conf 30 \
#     -stand_emit_conf 10 \
#     -nt 10 \
#     -o Combined2.vcf

# echo -e "Done...\n"    
# time=$( date "+%s")
# echo [elapsed time] $((($time - $time0bis)/60)) min

# rm -rf tmp
#--------------------------------------------------------------------------------------------------  
echo -e "----SNPsExtraction----"
echo -e "Extracting the snps from the merged file \n"

java -jar $GATK_HOME/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R $REFGENOME/persicaV2.fasta \
    -V Combined2.vcf \
    -selectType SNP \
    -o raw_snps.vcf

echo -e "Done...\n"    
time=$( date "+%s")
echo [elapsed time] $((($time - $time0bis)/60)) min

#--------------------------------------------------------------------------------------------------  
echo -e "----SNPs_VARIANT_FILTRATION.sh----\n"
echo -e "Filtrating the snps found \n"

TMP=tmp_snps
java -Djava.io.tmpdir=$TMP -Xmx32G \
    -jar $GATK_HOME/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R $REFGENOME/persicaV2.fasta \
    -V raw_snps.vcf \
    --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" \
    --filterName "my_snp_filter" \
    -o Combined2_snps_filt.vcf
    
echo -e "Done...\n"    
time=$( date "+%s")
echo [elapsed time] $((($time - $time0bis)/60)) min

rm -rf tmp_snps

#--------------------------------------------------------------------------------------------------  
echo -e "----INDELs EXTRACTIOn----\n"
echo -e "Extracting the indels from the merged file \n"
java -jar $GATK_HOME/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R $REFGENOME/persicaV2.fasta \
    -V Combined2.vcf \
    -selectType INDEL \
    -o raw_indels.vcf 

echo -e "Done...\n"    
time=$( date "+%s")
echo [elapsed time] $((($time - $time0bis)/60)) min

#--------------------------------------------------------------------------------------------------  
echo -e "----Indels_VARIANT_FILTRATION.sh----"

echo -e "Filtrating the indels found \n"
TMP=tmp_indels
java -Djava.io.tmpdir=$TMP -Xmx32G \
    -jar $GATK_HOME/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R $REFGENOME/persicaV2.fasta \
    -V raw_indels.vcf \
    --filterExpression "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0 || InbreedingCoeff < -0.8 || SOR > 10" \
    --filterName "my_snp_filter" \
    -o Combined2_indels_filt.vcf

echo -e "Done...\n"    
time=$( date "+%s")
echo [elapsed time] $((($time - $time0bis)/60)) min

rm -rf tmp_indels
